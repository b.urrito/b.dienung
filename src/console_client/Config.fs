module ConsoleClient.Configuration
open Newtonsoft.Json

let private defaultConfigFile = ".burrito"

type Configuration() = 
    let mutable apiKey: string = ""
    let mutable host: string = "http://localhost:5000/api"

    member this.ApiKey
        with get() = apiKey
        and set(value) = apiKey <- value

    member this.Host
        with get() = host
        and set(value) = host <- value

let private getConfig: Configuration option =
    if System.IO.File.Exists(defaultConfigFile) then 
        let content = System.IO.File.ReadAllText(defaultConfigFile)
        Some (JsonConvert.DeserializeObject<Configuration>(content))
    else
        None
        
let setConfig config = 
    let settings = new JsonSerializerSettings()
    settings.ContractResolver <- Serialization.CamelCasePropertyNamesContractResolver()
    let content =JsonConvert.SerializeObject(config)        
    System.IO.File.WriteAllText(defaultConfigFile, content)
        
let getConfigOrDefault: Configuration =
    match getConfig with 
    | Some configuration -> 
        configuration
    | None ->
        Configuration()
