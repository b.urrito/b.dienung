#!/usr/bin/env bash

CYAN=`tput setaf 6`
RESET=`tput sgr0`

#
# Git tag
#
echo "${CYAN}Creating new tag.${RESET}"
#get highest tag number
VERSION=`git describe --abbrev=0 --tags`

#replace . with space so can split into an array
VERSION_BITS=(${VERSION//./ })

#get number parts and increase last one by 1
VNUM1=${VERSION_BITS[0]}
VNUM2=${VERSION_BITS[1]}
VNUM3=${VERSION_BITS[2]}
VNUM3=$((VNUM3+1))

#create new tag
NEW_TAG="$VNUM1.$VNUM2.$VNUM3"

echo "Updating $VERSION to $NEW_TAG"

#get current hash and see if it already has a tag
GIT_COMMIT=`git rev-parse HEAD`
NEEDS_TAG=`git describe --contains $GIT_COMMIT`

#only tag if no tag already (would be better if the git describe command above could have a silent option)
if [ -z "$NEEDS_TAG" ]; then
    echo "Tagged with $NEW_TAG (Ignoring fatal:cannot describe - this means commit is untagged) "
    git tag $NEW_TAG
    git push --tags
else
    echo "Already a tag on this commit"
    echo "Will update the existing tag ($VERSION) with new contents."
    NEW_TAG=$VERSION
fi

#
# Create releases
#
echo -e "${CYAN}Cleaning solution.${RESET}"
dotnet clean
echo -e "${CYAN}Removing old output directory.${RESET}"
rm -rf out

VERSIONS=(fedora-x64 ubuntu-x64 linux-x64 win10-x64 osx-x64)
#VERSIONS=(fedora-x64)
MARKDOWN_URLS=
for ARCHITECTURE in ${VERSIONS[@]}; do
    echo -e "${CYAN}Publishing for $ARCHITECTURE.${RESET}"
    dotnet publish -c Release -o out/$ARCHITECTURE --self-contained --runtime $ARCHITECTURE

    cd out/$ARCHITECTURE
    ZIP_FILENAME="burrito_${ARCHITECTURE}_${NEW_TAG}.zip"
    echo -e "${CYAN}Zipping $ARCHITECTURE release as $ZIP_FILENAME.${RESET}"
    zip -r $ZIP_FILENAME *
    URL=$(curl -s --request POST --header "Private-Token: zebxmphUR7audGPQt-He" --form "file=@$ZIP_FILENAME" https://gitlab.com/api/v4/projects/9879684/uploads | jq '.markdown' | sed -e 's/^"//' -e 's/"$//')
    MARKDOWN_URLS="${MARKDOWN_URLS} ${URL}"
    cp $ZIP_FILENAME ../
    cd ../..
done
cp out/**/burrito*.zip out

echo -e "${CYAN}Editing release notes to release: $NEW_TAG.${RESET}"

echo -e "${CYAN}Checking for a previous release.${RESET}"
OLD_RELEASE=$(curl -s -H 'Accept: application/json' --header "Private-Token: zebxmphUR7audGPQt-He" https://gitlab.com/api/v4/projects/9879684/repository/tags/$NEW_TAG/ | jq '.release')

if [ "$OLD_RELEASE" = "null" ]; then
    echo -e "${CYAN}The release will be created with: ${MARKDOWN_URLS}${RESET}"
    curl    -H 'Accept: application/json' \
            -H 'Content-Type: application/json'\
            -X POST \
            -d "{\"description\": \"${MARKDOWN_URLS[@]}\"}" \
            -s \
            --header "Private-Token: zebxmphUR7audGPQt-He" \
            https://gitlab.com/api/v4/projects/9879684/repository/tags/$NEW_TAG/release
else
    echo -e "${CYAN}The release will be updated with: ${MARKDOWN_URLS}${RESET}"
    curl    -H 'Accept: application/json' \
            -H 'Content-Type: application/json'\
            -X PUT \
            -d "{\"description\": \"${MARKDOWN_URLS[@]}\"}" \
            -s \
            --header "Private-Token: zebxmphUR7audGPQt-He" \
            https://gitlab.com/api/v4/projects/9879684/repository/tags/$NEW_TAG/release
fi

