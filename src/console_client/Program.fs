﻿open System
open System.Reflection
open Argu
open Backend.SharedEntities
open ConsoleClient
open ConsoleClient.Arguments
open ConsoleClient.Configuration
open Newtonsoft.Json
open ConsoleClient

let printAndRead (description: string): string =
    do printf "%s" description
    Console.ReadLine()
    
let printLineAndRead (description: string): string =
    printAndRead (description + System.Environment.NewLine)

let loginFromConsole username (config: Configuration) = 
    do printfn "Please enter the password for user '%s':" username
    let password = Console.ReadLine ()
    let loginResult = ConsoleClient.Http.login config (new Backend.SharedEntities.Models.LoginCredentials(username, password))
    match loginResult with 
    | Ok content -> 
        config.ApiKey <- content
        setConfig config
        Ok "Operation successful"
    | Error ex   -> 
        Error ex.Message

let loginState (config: Configuration) =
    match String.IsNullOrWhiteSpace(config.ApiKey) with 
    | true -> Error "Could not find api key."
    | false -> Ok "Found api key."
    
let createRegistration (config: Configuration) =
    let name = printAndRead "Please enter a device name: "
    let location = printAndRead "Please enter a device location: "
    let calendar = printAndRead "Please enter an ical url: "
    let registration = Backend.SharedEntities.Models.Registration(
                        Name = name,
                        Location = location,
                        Calendar = calendar
                        )
    let result = Http.createRegistration config registration
    match result with 
    | Ok response ->
        printfn "%A" response
        Ok "Created new registration."
    | Error e ->
        Error e.Message

let cutString (indicator: string) maxLength (s: string) =
    if s.Length > maxLength then    
        s.Substring(0, maxLength - indicator.Length) + indicator
    else
        s

let dotCutString = cutString "..."
    
let padString width (s: string) =
    s.PadRight(width)

let padOrCut maxWidth = dotCutString maxWidth >> padString maxWidth

let printDevices (devices: Models.Device list) =
    let totalWidth = Math.Max(Console.BufferWidth, 80)
    let lastActiveLength = 11
    let errorsLength = 3
    let separator = " "
    let idLength = 36
    let maxNameLength = (totalWidth - (4 * separator.Length) - idLength - lastActiveLength - errorsLength) / 2
    let maxLocationLength = (totalWidth - (4 * separator.Length) - idLength - lastActiveLength - errorsLength) / 2
    let header = "ID".PadRight(idLength) + separator +
                 "NAME".PadRight(maxNameLength) + separator + 
                 "LOCATION".PadRight(maxLocationLength) + separator + 
                 "LAST-ACTIVE".PadRight(lastActiveLength) + separator +
                 "ERR"
    printfn "%s" header
    printfn "%s" <| String('=', header.Length)
    devices |> List.iter (fun device ->
                            (
                                let row = (device.Id.ToString() |> padOrCut idLength) +
                                          String(' ', separator.Length) + 
                                          (device.Name |> padOrCut maxNameLength) + 
                                          String(' ', separator.Length) + 
                                          (device.Location |> padOrCut maxLocationLength) +
                                          String(' ', separator.Length) + 
                                          (device.LastActive.ToString("HH:mm dd/MM") |> padOrCut lastActiveLength) +
                                          String(' ', separator.Length) + 
                                          if device.Errors.Count = 0 then "NO " else "YES"
                                do printfn "%s" row
                            ))

let printDeviceDetails (device: Models.Device) =
    do printfn "Id:          %A" device.Id
    do printfn "Name:        %s" device.Name
    do printfn "Location:    %s" device.Location
    do printfn "Timezone:    %s" device.TimeZone
    do printfn "Calendar:    %s" device.Calendar
    do printfn "Last active: %s" <| device.LastActive.ToString("HH:mm dd.MM.yyyy")
    do printfn "Errors:"
    match device.Errors.Count with
    | 0 -> printfn "\tNONE"
    | _ -> device.Errors |> Seq.iter (fun e -> (printfn "\t%s - %s" (e.Timestamp.ToString("HH:mm dd.MM.yyyy")) e.Description))

let getAllDevices (config: Configuration) =
    let result = Http.getAllDevices config
    match result with 
    | Ok response ->
        let devices = JsonConvert.DeserializeObject<List<Models.Device>>(response)
        printDevices devices
        let message = (sprintf "Read %s device%s from remote." (devices.Length.ToString()) (if devices.Length = 1 then "" else "s"))
        Ok message
    | Error e ->
        Error e.Message

let getDeviceById (config: Configuration) (id: string) =
    let result = Http.getDeviceById config id
    match result with
    | Ok response ->
        let device = JsonConvert.DeserializeObject<Models.Device>(response)
        printDeviceDetails device
        Ok "Details read successfully."
    | Error e ->    
        Error e.Message

let modifyDeviceById (config: Configuration) (id: string) =
    do printfn "If you do not want to change a particular field keep it empty."
    do printfn "Only modified values will be persisted."
    let name = printAndRead "Please enter new device name: "
    let location = printAndRead "Please enter new device location: "
    let calendar = printAndRead "Please enter the new calendar url: "
    let device = Models.Device (Name = name, Location = location, Calendar = calendar)
    let result = Http.modifyDeviceById config id device
    match result with
    | Ok response ->
        Ok "Device has been updated successfully. Use the devices subcommand to check the details."
    | Error e ->
        Error e.Message

let deleteDeviceById (config: Configuration) (id: string) =
    let result = Http.deleteDeviceById config id
    match result with
    | Ok _ ->
        Ok "The device was removed. Use the devices subcommand to check."
    | Error e ->
        Error e.Message

let getAllUsers (config: Configuration) =
    let result = Http.getAllUsers config
    match result with
    | Ok serializedUsers ->
        let users = JsonConvert.DeserializeObject<List<Models.User>>(serializedUsers)
        do printfn "%s %s" ("ID".PadRight(5)) "NAME"
        do printfn "%s" <| String('=', Console.BufferWidth)
        users |> Seq.iter (fun user -> printfn "%s %s" (user.Id.ToString().PadRight(5)) user.Name)
        Ok (sprintf "Read %s user%s from remote." (users.Length.ToString()) (if users.Length = 1 then "" else "s"))
    | Error e ->    
        Error e.Message

let getPrerenderedLayout (config: Configuration) (deviceId: string) =
    let maxWidth = 80
    let result = Http.getPrerenderedCalendar config deviceId
    match result with
    | Ok layout ->
        let lines = JsonConvert.DeserializeObject<List<Models.PrerenderedCalendarLine>>(layout)
        lines |> Seq.iter (fun line -> 
                    match line.BackgroundColor with
                    | Models.LineColors.Black  -> Console.BackgroundColor <- ConsoleColor.Black
                    | Models.LineColors.White  -> Console.BackgroundColor <- ConsoleColor.White
                    | Models.LineColors.Yellow -> Console.BackgroundColor <- ConsoleColor.Yellow
                    | _ -> failwith "Unknown color value."
                    match line.ForegroundColor with
                    | Models.LineColors.Black  -> Console.ForegroundColor <- ConsoleColor.Black
                    | Models.LineColors.White  -> Console.ForegroundColor <- ConsoleColor.White
                    | Models.LineColors.Yellow -> Console.ForegroundColor <- ConsoleColor.Yellow
                    | _ -> failwith "Unknown color value."
                    do printf "%s" <| line.Content.PadRight(maxWidth)
                    do Console.ResetColor ()
                    do printfn ""
                )
        Ok ""
    | Error ex ->
        Error ex.Message

[<EntryPoint>]
let main argv =
    let errorHandler = ProcessExiter(colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red)
    let parser = ArgumentParser.Create<CLIArguments>(errorHandler = errorHandler)
    let config = ConsoleClient.Configuration.getConfigOrDefault

    match argv.Length with 
    | 0 ->
        printfn "No arguments supplied. You can either run this tool by specifying options (like login) or use a subcommand. Please check the following instruction on how to use this tool:"
        parser.PrintUsage () |> printf "%s"
        1
    | _ ->
        try
            let args = parser.ParseCommandLine(inputs = argv, raiseOnUsage = true)
            let firstArg = args.GetAllResults () |> List.head
            
            let (result: Result<string, string>) = 
                match firstArg with 
                | Version ->
                    let version = Assembly.GetExecutingAssembly().GetName().Version.ToString()
                    Ok (sprintf "Version: %s" version)
                | Login arg ->
                    match arg with 
                    | Some name -> (loginFromConsole name config)
                    | None      -> (loginState config)
                | Host arg ->
                    match Uri.TryCreate(arg, UriKind.Absolute) with
                    | true, uri ->
                        do config.Host <- uri.ToString().TrimEnd('/')
                        do setConfig config
                        Ok "Set a new host url."
                    | _ ->
                        Error "The url is not recognized."
                | Devices subCommand ->
                    let subCommand = subCommand.GetAllResults () |> List.head
                    match subCommand with
                    | DevicesArgs.Add ->
                        createRegistration config
                    | DevicesArgs.List ->
                        getAllDevices config
                    | DevicesArgs.Modify deviceId ->
                        modifyDeviceById config deviceId
                    | DevicesArgs.Remove deviceId ->
                        deleteDeviceById config deviceId
                    | DevicesArgs.Details deviceId ->
                        getDeviceById config deviceId
                    | DevicesArgs.Preview deviceId ->
                        getPrerenderedLayout config deviceId
                | Users subCommand ->
                    let subCommand = subCommand.GetAllResults () |> List.head
                    match subCommand with
                    | UsersArgs.Add ->
                        Ok "The backend is currently running with a static user management. No user operations are available."
                    | UsersArgs.List ->
                        getAllUsers config
                    | UsersArgs.Remove userId ->
                        Ok "The backend is currently running with a static user management. No user operations are available."
                        
            match result with 
            | Ok success ->
                printfn "%s" success
            | Error error ->
                Console.ForegroundColor <- ConsoleColor.Red
                printfn "%s" error
                Console.ResetColor ()
                
        with e ->
            printfn "%s" e.Message
        0      
    
