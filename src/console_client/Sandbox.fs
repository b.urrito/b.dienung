module ConsoleClient.Sandbox
open System

let operation1 (input: string): Result<string, string> =
    if System.String.IsNullOrWhiteSpace(input) then 
        Error "argument must not be null"
    else 
        Ok (input + " operation #1")
        
let operation2 (input: string): Result<string, string> =
    if System.String.IsNullOrWhiteSpace input then 
        Error "argument must not be null"
    else 
        Ok (input + " operation #2")
        
