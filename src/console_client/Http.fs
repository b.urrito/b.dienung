module ConsoleClient.Http

open System
open System.Text
open System.Web
open HttpFs.Client
open Hopac
open Hopac.Ch
open HttpFs.Client
open Newtonsoft.Json
open Backend.SharedEntities
open ConsoleClient
open ConsoleClient.Configuration

type HttpResult = Result<string, Exception>

let jsonFormatter input =
    JsonConvert.SerializeObject input    

let get baseUrl endpoint =
    Request.createUrl Get (baseUrl + "/" + endpoint)
    |> Request.responseCharacterEncoding Encoding.UTF8    
    |> Request.responseAsString
    |> run

let getWithAuthorization baseUrl apiKey endpoint =
    Request.createUrl Get (baseUrl + "/" + endpoint)
    |> Request.setHeader (Custom ("api_key", apiKey))
    |> Request.responseCharacterEncoding Encoding.UTF8
    |> Request.responseAsString
    |> run
    
let getRequest (baseUrl: string) (apiKey: string) (endpoint: string) (parameters: (string*string) seq option) =
    let uri = baseUrl + "/" + endpoint
    let builder = UriBuilder(uri)
    let query = HttpUtility.ParseQueryString(builder.Query)
    let parameters = match parameters with 
                     | Some p -> p
                     | None   -> Seq.empty<string*string>
    parameters |> Seq.iter (fun (key, value) -> query.[key] <- value)
    builder.Query <- query.ToString()
    Request.createUrl Get (builder.ToString())
    |> Request.setHeader (Custom ("api_key", apiKey))
    
let postRequest (outgoingBodyFormatter: obj -> string) baseUrl apiKey endpoint body =
    let content = outgoingBodyFormatter body
    Request.createUrl Post (baseUrl + "/" + endpoint)
    |> Request.body (BodyString content)
    |> Request.setHeader (ContentType (ContentType.create("application", "json")))
    |> Request.setHeader (Custom ("api_key", apiKey))
    
let putRequest (outgoingBodyFormatter: obj -> string) baseUrl apiKey endpoint body =
    let content = outgoingBodyFormatter body
    Request.createUrl Put (baseUrl + "/" + endpoint)
    |> Request.body (BodyString content)
    |> Request.setHeader (ContentType (ContentType.create("application", "json")))
    |> Request.setHeader (Custom ("api_key", apiKey))

let deleteRequest baseUrl apiKey endpoint =
    let uri = baseUrl + "/" + endpoint
    Request.createUrl Delete uri
    |> Request.setHeader (Custom ("api_key", apiKey))

let runJob request =
    job {
        use! response = getResponse request
        let! bodyAsString = Response.readBodyAsString response
        return bodyAsString
    }
       
let sendRequest (responseBodyFormatter: string -> string) (request: Request): Async<HttpResult> =
    printfn "Sending request to: %s" <| request.url.ToString()
    let asyncJob = job {
        try
            use! response = getResponse request
            let code = response.statusCode
            if code < 200 || code > 299 then
                return failwith ("Returned status code: " + code.ToString())
            else
                let! bodyAsString = Response.readBodyAsString response
                let formattedBody = responseBodyFormatter bodyAsString
                return Ok formattedBody
        with 
        | ex ->
            return (Error ex)
    }
    asyncJob |> Job.toAsync
    
let sendRequestSync (responseBodyFormatter: string -> string) (request: Request): HttpResult =
    (sendRequest responseBodyFormatter request) |> Async.RunSynchronously
    
let login (configuration: Configuration) (credentials: Models.LoginCredentials) =
    postRequest jsonFormatter configuration.Host "" "users/login" credentials
    |> sendRequestSync id

let createRegistration (configuration: Configuration) (registration: Models.Registration) =
    postRequest jsonFormatter configuration.Host configuration.ApiKey "registrations" registration
    |> sendRequestSync id
    
let getAllDevices (configuration: Configuration) =
    getRequest configuration.Host configuration.ApiKey "devices" None
    |> sendRequestSync id

let getDeviceById (configuration: Configuration) (deviceId: string) =
    let url = ("devices/" + deviceId)
    getRequest configuration.Host configuration.ApiKey url None
    |> sendRequestSync id

let modifyDeviceById (configuration: Configuration) (deviceId: string) (device: Models.Device) =
    let url = (configuration.Host + "/devices/" + deviceId)
    putRequest jsonFormatter configuration.Host configuration.ApiKey url device
    |> sendRequestSync id

let deleteDeviceById (configuration: Configuration) (deviceId: string) =
    let url = ("devices/" + deviceId)
    deleteRequest configuration.Host configuration.ApiKey url
    |> sendRequestSync id

let getAllUsers (configuration: Configuration) =
    let url = ("users")
    getRequest configuration.Host configuration.ApiKey url None
    |> sendRequestSync id

let getPrerenderedCalendar (configuration: Configuration) (deviceId: string) =
    let url = ("devices/" + deviceId + "/calendar/prerenderedEvents/")
    getRequest configuration.Host configuration.ApiKey url None
    |> sendRequestSync id