module ConsoleClient.Arguments

open Argu

type DevicesArgs =
    | [<CliPrefix(CliPrefix.None)>]                           [<Mandatory>] Add 
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("rm")>]  [<Mandatory>] Remove of string
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("mod")>] [<Mandatory>] Modify of string
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("ls")>]  [<Mandatory>] List
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("v")>]   [<Mandatory>] Details of string
    | [<CliPrefix(CliPrefix.None)>]                           [<Mandatory>] Preview of string
    with
        interface IArgParserTemplate with 
            member this.Usage =
                match this with 
                | Add       -> "adds a new device"
                | Remove _  -> "removes an existing device, supply a Guid or '*' to remove all"
                | Modify _  -> "modifies name/location/calendar of a device, supply a Guid to select a device"
                | List _    -> "lists all devices"
                | Details _ -> "gets details for the device with the given id"
                | Preview _ -> "retrieves a prerendered calendar view for the griven device"
    
type UsersArgs =
    | [<CliPrefix(CliPrefix.None)>]                           [<Mandatory>] Add 
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("rm")>]  [<Mandatory>] Remove of string
    | [<CliPrefix(CliPrefix.None)>] [<AltCommandLine("ls")>]  [<Mandatory>] List
    with
        interface IArgParserTemplate with 
            member this.Usage =
                match this with 
                | Add       -> "adds a new user"
                | Remove _  -> "removes an existing user, supply an id"
                | List _    -> "lists all users"
                
type CLIArguments =
    | [<CliPrefix(CliPrefix.None)>] [<First>] Version
    | [<CliPrefix(CliPrefix.None)>] [<First>] Login   of string option
    | [<CliPrefix(CliPrefix.None)>] [<First>] Host    of string
    | [<CliPrefix(CliPrefix.None)>] [<First>] Devices of ParseResults<DevicesArgs>
    | [<CliPrefix(CliPrefix.None)>] [<First>] Users   of ParseResults<UsersArgs>
    with 
        interface IArgParserTemplate with 
            member this.Usage =
                match this with
                | Version   -> "echos the version of this tool"
                | Login _   -> "retrieves a session token from the remote api, you need to supply your username as parameter"
                | Host _    -> "sets the url of the remote host"
                | Devices _ -> "adds/removes/modifies devices"
                | Users _   -> "adds/removes users"

